#include "compiler_abstraction.h"


/* Interrupt handler prototypes */
void __WEAK Reset_Handler (void);
void __WEAK NMI_Handler (void);
void __WEAK HardFault_Handler (void);
void __WEAK MemoryManagementFault_Handler (void);
void __WEAK BusFault_Handler (void);
void __WEAK UsageFault_Handler (void);
void __WEAK SecureFault_Handler (void);
void __WEAK SVCall_Handler (void);
void __WEAK Debug_Handler (void);
void __WEAK PendSV_Handler (void);
void __WEAK SysTick_Handler (void);

void __WEAK SPU_IRQHandler (void);
void __WEAK CLOCK_POWER_IRQHandler (void);
void __WEAK UARTE0_SPIM0_SPIS0_TWIM0_TWIS0_IRQHandler (void);
void __WEAK UARTE1_SPIM1_SPIS1_TWIM1_TWIS1_IRQHandler (void);
void __WEAK UARTE2_SPIM2_SPIS2_TWIM2_TWIS2_IRQHandler (void);
void __WEAK UARTE3_SPIM3_SPIS3_TWIM3_TWIS3_IRQHandler (void);
void __WEAK GPIOTE0_IRQHandler (void);
void __WEAK SAADC_IRQHandler (void);
void __WEAK TIMER0_IRQHandler (void);
void __WEAK TIMER1_IRQHandler (void);
void __WEAK TIMER2_IRQHandler (void);
void __WEAK RTC0_IRQHandler (void);
void __WEAK RTC1_IRQHandler (void);
void __WEAK WDT_IRQHandler (void);
void __WEAK EGU0_IRQHandler (void);
void __WEAK EGU1_IRQHandler (void);
void __WEAK EGU2_IRQHandler (void);
void __WEAK EGU3_IRQHandler (void);
void __WEAK EGU4_IRQHandler (void);
void __WEAK EGU5_IRQHandler (void);
void __WEAK PWM0_IRQHandler (void);
void __WEAK PWM1_IRQHandler (void);
void __WEAK PWM2_IRQHandler (void);
void __WEAK PWM3_IRQHandler (void);
void __WEAK PDM_IRQHandler (void);
void __WEAK I2S_IRQHandler (void);
void __WEAK IPC_IRQHandler (void);
void __WEAK FPU_IRQHandler (void);
void __WEAK GPIOTE1_IRQHandler (void);
void __WEAK KMU_IRQHandler (void);
void __WEAK CRYPTOCELL_IRQHandler (void);

/* Exported constants from the linker script */
__UNUSED extern unsigned long _start_text;      // Start of the program code
extern unsigned long _end_text;         // End of the program code
extern unsigned long _start_data;       // Start of the initialized data section (values to be copied from FLASH into RAM)
extern unsigned long _end_data;         // End of the initialized data section
extern unsigned long _start_bss;        // Start of the uninitialized data section (to be initialized with zeroes)
extern unsigned long _end_bss;          // End of the uninitialized data section
extern unsigned long _stack_top;        // Top of the stack

/* Define stack and heap */
__SECTION(".stack")
unsigned int stack[8192 / 4];

__SECTION(".heap")
unsigned int heap[0];

/* Interrupt vector table */
__SECTION(".isr_vector")
void (* const vectors[])(void) = 
{
    (void (*)(void))&_stack_top,                  /* Top of Stack: Initial Stack Pointer (SP) value */
    Reset_Handler,
    NMI_Handler,
    HardFault_Handler,
    MemoryManagementFault_Handler,
    BusFault_Handler,
    UsageFault_Handler,
    SecureFault_Handler,
    0,
    0,
    0,
    SVCall_Handler,
    Debug_Handler,
    0,
    PendSV_Handler,
    SysTick_Handler,
    /* End of the Cortex-M33 interrupt vectors */
    /* Start of the nRF9160 interrupt vectors */
    0,
    0,
    0,
    SPU_IRQHandler,
    0,
    CLOCK_POWER_IRQHandler,
    0,
    0,
    UARTE0_SPIM0_SPIS0_TWIM0_TWIS0_IRQHandler,
    UARTE1_SPIM1_SPIS1_TWIM1_TWIS1_IRQHandler,
    UARTE2_SPIM2_SPIS2_TWIM2_TWIS2_IRQHandler,
    UARTE3_SPIM3_SPIS3_TWIM3_TWIS3_IRQHandler,
    0,
    GPIOTE0_IRQHandler,
    SAADC_IRQHandler,
    TIMER0_IRQHandler,
    TIMER1_IRQHandler,
    TIMER2_IRQHandler,
    0,
    0,
    RTC0_IRQHandler,
    RTC1_IRQHandler,
    0,
    0,
    WDT_IRQHandler,
    0,
    0,
    EGU0_IRQHandler,
    EGU1_IRQHandler,
    EGU2_IRQHandler,
    EGU3_IRQHandler,
    EGU4_IRQHandler,
    EGU5_IRQHandler,
    PWM0_IRQHandler,
    PWM1_IRQHandler,
    PWM2_IRQHandler,
    PWM3_IRQHandler,
    0,
    PDM_IRQHandler,
    0,
    I2S_IRQHandler,
    0,
    IPC_IRQHandler,
    0,
    FPU_IRQHandler,
    0,
    0,
    0,
    0,
    GPIOTE1_IRQHandler,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    KMU_IRQHandler,
    0,
    0,
    0,
    0,
    0,
    0,
    CRYPTOCELL_IRQHandler,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,	
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
	0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0,
    0
};


/* Functions */

void __WEAK SystemInit (void)
{
    
}

int __WEAK main (void)
{
    int i = 0;
    while(1)
    {
        i++;
    }
}

void Reset_Handler (void)
{
    /* Copy the initialized data values from FLASH; values are located right after the program code */
    unsigned long *source = &_end_text;
    unsigned long *destination = &_start_data;
    
    while(destination < &_end_data)
    {
        *(destination++) = *(source++);
    }
    
    /* Set the uninitialized data to zeroes */
    destination = &_start_bss;
    while(destination < &_end_bss)
    {
        *(destination++) = 0;
    }
    
    /* Call the system initialization function */
    SystemInit();
    
    /* Call the main function */
    main();
}
