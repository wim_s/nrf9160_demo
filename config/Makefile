###############
#Project name #
###############
PROJECT_NAME	:= nrf9160_Demo

######################
# Toolchain commands #
######################
TOOLCHAIN_PATH 	:= C:/Embedded/gcc/bin/
GNU_PREFIX 		:= $(TOOLCHAIN_PATH)arm-none-eabi
CC      		:= $(GNU_PREFIX)-gcc
CXX     		:= $(GNU_PREFIX)-c++
AS      		:= $(GNU_PREFIX)-as
AR      		:= $(GNU_PREFIX)-ar -r
LD      		:= $(GNU_PREFIX)-ld
NM      		:= $(GNU_PREFIX)-nm
OBJDUMP 		:= $(GNU_PREFIX)-objdump
OBJCOPY 		:= $(GNU_PREFIX)-objcopy
SIZE    		:= $(GNU_PREFIX)-size
READELF     	:= $(GNU_PREFIX)-readelf

################
#Linker script #
################
LINKER_SCRIPT  	:= nrf9160.ld

###########
# Folders #
###########
PROJ_ROOT_DIR 	:= ../
OUTPUT_DIR 		:= $(PROJ_ROOT_DIR)_output/
OBJ_DIR			:= $(PROJ_ROOT_DIR)_objects/
SRC_DIR			:= $(PROJ_ROOT_DIR)source/
INC_DIR			:= $(PROJ_ROOT_DIR)include/
RTOS_DIR		:= $(PROJ_ROOT_DIR)FreeRTOS/

###############
# Output file #
###############
TARGET			:= $(OUTPUT_DIR)$(PROJECT_NAME).elf
MAPFILE			:= $(OUTPUT_DIR)$(PROJECT_NAME).map

################
# Source files #
################
# search for all C source files within the SRC_DIR directory
SRC_FILES 		:= $(shell find $(SRC_DIR) -name "*.c")
# search for all C source files within the RTOS_DIR directory but excluding the secure port
#SRC_FILES 		+= $(shell find $(RTOS_DIR)Source/ -type f -name "*.c" ! -path "$(RTOS_DIR)Source/portable/GCC/ARM_CM33/*")


###################
# Include folders #
###################
INC_FOLDERS 	+= \
				$(INC_DIR)nRF9160 \
				$(RTOS_DIR) \
				$(RTOS_DIR)Source/include/ \
				$(RTOS_DIR)Source/portable/GCC/ARM_CM33_NTZ/non_secure/
	
INC_PARAMS 		:= $(foreach d, $(INC_FOLDERS), -I$d)

###################################
# Libraries common to all targets #
###################################
LIB_FILES 		+= 

################
# Object files #
################
OBJECTS 		:= $(patsubst $(SRC_DIR)%.c, $(OBJ_DIR)%.o, $(SRC_FILES))
OBJ_DIRS 		:= $(sort $(dir $(OBJECTS)))

#####################################
# Optimization & debug symbol flags #
#####################################
# optimize for debugging; see https://gcc.gnu.org/onlinedocs/gcc/Optimize-Options.html
OPT 			:= -Og
# include debugging symbols with extra information (level 3)
OPT 			+= -g3

####################
# C compiler flags #
####################
CFLAGS 			+= $(OPT)
CFLAGS 			+= -DNRF9160_XXAA
CFLAGS 			+= -std=gnu99
# show all warnings and treat them as errors
CFLAGS 			+= -Wall -Wextra -Werror			
# generate stack usage information
CFLAGS 			+= -fstack-usage
# set architecture to ARM v8 mainline with DSP disabled and FPU disabled
CFLQGS 			+= -march=armv8-m.main+nodsp+nofp
# set CPU to Cortex-M33
CFLAGS 			+= -mcpu=cortex-m33
CFLAGS 			+= -mthumb -mabi=aapcs				
CFLAGS 			+= -mfloat-abi=hard -mfpu=auto

######################
# C++ compiler flags #
######################
CXXFLAGS 		+= $(OPT)

###################
# Assembler flags #
###################
ASMFLAGS 		+= $(CFLAGS)

################
# Linker flags #
################
LDFLAGS 		+= $(OPT)
LDFLAGS 		+= -mthumb -mabi=aapcs -T$(LINKER_SCRIPT)
LDFLAGS 		+= -mcpu=cortex-m33
LDFLAGS 		+= -mfloat-abi=hard -mfpu=auto
# let the linker place each function in its own section
LDFLAGS 		+= -ffunction-sections
# let the linker dump unused sections
LDFLAGS 		+= -Wl,--gc-sections
# let the linker show which sections are dumped
#LDFLAGS += -Wl,--print-gc-sections


# Add standard libraries at the very end of the linker input, after all objects
# that may need symbols provided by these libraries.
LIB_FILES += -lc -lnosys -lm

#################
# Build targets #
#################
.PHONY: all
all: create_output_dirs $(SRC_FILES) $(TARGET)
	@echo $(SRC_FILES)
$(TARGET): $(OBJECTS) 
	$(CC) $(OBJECTS) $(INC_PARAMS) $(LDFLAGS) -Wl,-Map=$(MAPFILE) -o $(TARGET)
	$(OBJCOPY) -O ihex $(TARGET) $(OUTPUT_DIR)/$(PROJECT_NAME).hex
	$(OBJCOPY) -O binary $(TARGET) $(OUTPUT_DIR)/$(PROJECT_NAME).bin
	$(SIZE) $(TARGET)

$(OBJ_DIR)%.o: $(SRC_DIR)%.cpp
	$(CXX) $(CXXFLAGS) $(INC_PARAMS) -c $< -o $@

$(OBJ_DIR)%.o: $(SRC_DIR)%.c
	$(CC) $(CFLAGS) $(INC_PARAMS) -c $< -o $@

$(OBJ_DIR)%.o: $(SRC_DIR)%.s
	$(AS) -x assembler-with-cpp $(ASMFLAGS) $(INC_PARAMS) $< -o $@

.PHONY: clean
clean:
	rm -rf $(OBJ_DIR)* $(OUTPUT_DIR)*

#######################################
# Function: Create output directories #
#######################################
create_output_dirs:
	$(shell mkdir -p $(OUTPUT_DIR))
	$(shell mkdir -p $(OBJ_DIRS))

